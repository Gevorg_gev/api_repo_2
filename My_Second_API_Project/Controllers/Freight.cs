using System.Runtime.InteropServices;
using System;

namespace My_Second_API_Project
{
    public class Freight
    {
        public DateTime DateOfFreight { get; set; }

        public int CountOfFreight { get; set; }

        public string PriceOfFreight { get; set; } 

        public int MassOfFreight { get; set; } 

        public string FreightDemand { get; set; } 
    }
}
