using System.Runtime.InteropServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace My_Second_API_Project.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class FreightController : ControllerBase
    {
        private static readonly string[] Price_Of_Freights = new[]
        {
            "$1000" , "$1280" , "$495" , "$789" , "$4589" , "$7845"
        };

        public static readonly string[] Demand_Of_Freights = new[]
        {
            "High" , "Low" 
        };

        private readonly ILogger<FreightController> controller;

        public FreightController(ILogger<FreightController> controller)
        {
            this.controller = controller;
        }

        [HttpGet]

        public IEnumerable<Freight> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new Freight
            {
                DateOfFreight = DateTime.Now.AddDays(index),
                CountOfFreight = rng.Next(0, 5000),
                PriceOfFreight = Price_Of_Freights[Price_Of_Freights.Length],
                MassOfFreight = rng.Next(1,115),
                FreightDemand = Demand_Of_Freights[Demand_Of_Freights.Length] 
            })
            .ToArray();
        }
    }
}
